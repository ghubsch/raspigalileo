#!/bin/sh
rrdtool create weatherRRD.rrd \
        DS:tempo:GAUGE:600:-50:150 \
        DS:tempi:GAUGE:600:-50:150 \
        DS:humid:GAUGE:600:0:110 \
        DS:press:GAUGE:600:900:1050 \
        RRA:AVERAGE:0.5:1:600 \
        RRA:AVERAGE:0.5:6:700 \
        RRA:AVERAGE:0.5:24:775 \
        RRA:AVERAGE:0.5:288:797 \
        RRA:MIN:0.5:1:600 \
        RRA:MIN:0.5:6:700 \
        RRA:MIN:0.5:24:775 \
        RRA:MIN:0.5:288:797 \
        RRA:MAX:0.5:1:600 \
        RRA:MAX:0.5:6:700 \
        RRA:MAX:0.5:24:775 \
        RRA:MAX:0.5:288:797

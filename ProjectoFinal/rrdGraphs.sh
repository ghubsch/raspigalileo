#!/bin/sh

/usr/bin/rrdtool graph /home/pi/projecto/web/temp24h.png \
--height=200 --width=833 \
--start end-1d \
--vertical-label "Temperature (°C)" \
--title "Temperature - last 24h" \
--watermark "`date`" \
DEF:TempOUT=/home/pi/projecto/weatherRRD.rrd:tempo:AVERAGE \
DEF:TempIN=/home/pi/projecto/weatherRRD.rrd:tempi:AVERAGE \
DEF:TempOUTl=/home/pi/projecto/weatherRRD.rrd:tempo:AVERAGE:end=now-1d:start=end-1d \
DEF:TempINl=/home/pi/projecto/weatherRRD.rrd:tempi:AVERAGE:end=now-1d:start=end-1d \
CDEF:tTempOUT=TempOUT,1800,TRENDNAN \
CDEF:tTempIN=TempIN,1800,TRENDNAN \
CDEF:tTempOUTl=TempOUTl,1800,TRENDNAN \
CDEF:tTempINl=TempINl,1800,TRENDNAN \
SHIFT:tTempOUTl:86400 \
SHIFT:tTempINl:86400 \
VDEF:OUTpct=TempOUT,95,PERCENT \
VDEF:INpct=TempIN,95,PERCENT \
COMMENT:"                 Actual      " \
COMMENT:"Maximum      " \
COMMENT:"Average     " \
COMMENT:"Minimum     " \
COMMENT:"Percentil 95\l" \
AREA:tTempOUT#3366FF22 \
LINE1:tTempOUT#3366FF:"Exterior    " \
LINE1:tTempOUTl#A4A4A4::dashes=2,2 \
GPRINT:TempOUT:LAST:"%6.2lf %SºC  " \
GPRINT:TempOUT:MAX:"%6.2lf %SºC  " \
GPRINT:TempOUT:AVERAGE:"%6.2lf %SºC  " \
GPRINT:TempOUT:MIN:"%6.2lf %SºC  " \
GPRINT:OUTpct:"%6.2lf %SºC\l" \
AREA:tTempIN#FF660022 \
LINE1:tTempIN#FF6600:"Interior    " \
LINE1:tTempINl#A4A4A4::dashes=2,2 \
GPRINT:TempIN:LAST:"%6.2lf %SºC  " \
GPRINT:TempIN:MAX:"%6.2lf %SºC  " \
GPRINT:TempIN:AVERAGE:"%6.2lf %SºC  " \
GPRINT:TempIN:MIN:"%6.2lf %SºC  " \
GPRINT:INpct:"%6.2lf %SºC\l" 

/usr/bin/rrdtool graph /home/pi/projecto/web/temp7d.png \
--height=200 --width=833 \
--start end-7d \
--vertical-label "Temperature (°C)" \
--title "Temperature - Last 7 days" \
--watermark "`date`" \
DEF:TempOUT=/home/pi/projecto/weatherRRD.rrd:tempo:AVERAGE \
DEF:TempIN=/home/pi/projecto/weatherRRD.rrd:tempi:AVERAGE \
DEF:TempOUTl=/home/pi/projecto/weatherRRD.rrd:tempo:AVERAGE:end=now-1w:start=end-2w \
DEF:TempINl=/home/pi/projecto/weatherRRD.rrd:tempi:AVERAGE:end=now-1w:start=end-2w \
CDEF:tTempOUT=TempOUT,12600,TRENDNAN \
CDEF:tTempIN=TempIN,12600,TRENDNAN \
CDEF:tTempOUTl=TempOUTl,12600,TRENDNAN \
CDEF:tTempINl=TempINl,12600,TRENDNAN \
SHIFT:tTempOUTl:604800 \
SHIFT:tTempINl:604800 \
VDEF:OUTpct=TempOUT,95,PERCENT \
VDEF:INpct=TempIN,95,PERCENT \
COMMENT:"                 Actual      " \
COMMENT:"Maximum      " \
COMMENT:"Average     " \
COMMENT:"Minimum     " \
COMMENT:"Percentil 95\l" \
AREA:tTempOUT#3366FF22 \
LINE1:tTempOUT#3366FF:"Exterior    " \
LINE1:tTempOUTl#A4A4A4::dashes=2,2 \
GPRINT:TempOUT:LAST:"%6.2lf %SºC  " \
GPRINT:TempOUT:MAX:"%6.2lf %SºC  " \
GPRINT:TempOUT:AVERAGE:"%6.2lf %SºC  " \
GPRINT:TempOUT:MIN:"%6.2lf %SºC  " \
GPRINT:OUTpct:"%6.2lf %SºC\l" \
AREA:tTempIN#FF660022 \
LINE1:tTempIN#FF6600:"Interior    " \
LINE1:tTempINl#A4A4A4::dashes=2,2 \
GPRINT:TempIN:LAST:"%6.2lf %SºC  " \
GPRINT:TempIN:MAX:"%6.2lf %SºC  " \
GPRINT:TempIN:AVERAGE:"%6.2lf %SºC  " \
GPRINT:TempIN:MIN:"%6.2lf %SºC  " \
GPRINT:INpct:"%6.2lf %SºC\l" 

/usr/bin/rrdtool graph /home/pi/projecto/web/humidity24h.png \
--height=150 --width=350 \
--start end-1d \
--vertical-label "Relative humidity (%)" \
--title "Relative humidity - Last 24h" \
--watermark "`date`" \
DEF:humid=/home/pi/projecto/weatherRRD.rrd:humid:AVERAGE \
DEF:humidl=/home/pi/projecto/weatherRRD.rrd:humid:AVERAGE:end=now-1d:start=end-1d \
CDEF:thumid=humid,1800,TRENDNAN \
CDEF:thumidl=humidl,1800,TRENDNAN \
SHIFT:thumidl:86400 \
AREA:humid#4d993422 \
LINE1:thumid#4d9934:"Humidity" \
LINE1:thumidl#A4A4A4::dashes=2,2 \
GPRINT:humid:LAST:"Cur\:%3.0lf%%" \
GPRINT:humid:AVERAGE:"Avg\:%3.0lf%%" \
GPRINT:humid:MIN:"Min\:%3.0lf%%" \
GPRINT:humid:MAX:"Max\:%3.0lf%%\n" 

/usr/bin/rrdtool graph /home/pi/projecto/web/humidity7d.png \
--height=150 --width=350 \
--start end-7d \
--vertical-label "Relative humidity (%)" \
--title "Relative humidity - Last 7 days" \
--watermark "`date`" \
DEF:humid=/home/pi/projecto/weatherRRD.rrd:humid:AVERAGE \
DEF:humidl=/home/pi/projecto/weatherRRD.rrd:humid:AVERAGE:end=now-1w:start=end-2w \
CDEF:thumid=humid,12600,TRENDNAN \
CDEF:thumidl=humidl,12600,TRENDNAN \
SHIFT:thumidl:86400 \
AREA:humid#4d993422 \
LINE1:thumid#4d9934:"Humidity" \
LINE1:thumidl#A4A4A4::dashes=2,2 \
GPRINT:humid:LAST:"Cur\:%3.0lf%%" \
GPRINT:humid:AVERAGE:"Avg\:%3.0lf%%" \
GPRINT:humid:MIN:"Min\:%3.0lf%%" \
GPRINT:humid:MAX:"Max\:%3.0lf%%\n" 

/usr/bin/rrdtool graph /home/pi/projecto/web/press24h.png \
--height=150 --width=350 \
--start end-1d \
--upper-limit 1030 \
--lower-limit 950 \
--rigid \
--alt-y-grid \
--units-exponent 0 \
--vertical-label "Atmospheric Pressure (hPa)" \
--title "Atmospheric Pressure - Last 24h" \
--watermark "`date`" \
DEF:press=/home/pi/projecto/weatherRRD.rrd:press:AVERAGE \
DEF:pressl=/home/pi/projecto/weatherRRD.rrd:press:AVERAGE:end=now-1d:start=end-1d \
CDEF:tpress=press,1800,TRENDNAN \
CDEF:tpressl=pressl,1800,TRENDNAN \
SHIFT:tpressl:86400 \
AREA:tpress#B4040422 \
LINE1:tpress#B40404:"Pressure" \
LINE1:tpressl#A4A4A4::dashes=2,2 \
GPRINT:press:LAST:"Cur\:%4.1lf" \
GPRINT:press:AVERAGE:"Avg\:%4.1lf" \
GPRINT:press:MIN:"Min\:%4.1lf" \
GPRINT:press:MAX:"Max\:%4.1lf\n" 

/usr/bin/rrdtool graph /home/pi/projecto/web/press7d.png \
--height=150 --width=350 \
--start end-7d \
--upper-limit 1030 \
--lower-limit 950 \
--rigid \
--alt-y-grid \
--units-exponent 0 \
--vertical-label "Atmospheric Pressure (hPa)" \
--title "Atmospheric Pressure - Last 7 days" \
--watermark "`date`" \
DEF:press=/home/pi/projecto/weatherRRD.rrd:press:AVERAGE \
DEF:pressl=/home/pi/projecto/weatherRRD.rrd:press:AVERAGE:end=now-1w:start=end-2w \
CDEF:tpress=press,12600,TRENDNAN \
CDEF:tpressl=pressl,12600,TRENDNAN \
SHIFT:tpressl:604800 \
AREA:tpress#B4040422 \
LINE1:tpress#B40404:"Pressure" \
LINE1:tpressl#A4A4A4::dashes=2,2 \
GPRINT:press:LAST:"Cur\:%4.1lf" \
GPRINT:press:AVERAGE:"Avg\:%4.1lf" \
GPRINT:press:MIN:"Min\:%4.1lf" \
GPRINT:press:MAX:"Max\:%4.1lf\n"  


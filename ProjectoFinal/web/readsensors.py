#!/usr/bin/python
import commands
import sys
import re
import datetime
from Adafruit_BMP085 import BMP085

#------------------------------------------------------------------------
# readsensors.py
#------------------------------------------------------------------------

#Enable 1-wire GPIO
#os.system("modprobe w1-gpio")
#os.system("modprobe w1-therm")
#Enabled automatically adding these 2 lines to /etc/modules:
#   w1-gpio
#   w1-therm

#------------------------------------------------------------------------
# dbgprint - Print debug info (if program is started widh "-d" parameter
#------------------------------------------------------------------------
def dbgprint (vString):
	if len(sys.argv) > 1:
		if sys.argv[1] == "-d": 	#debug mode
			timestamp = datetime.datetime.now()
			print str(timestamp) + " - " + vString

#------------------------------------------------------------------------
# readDS18B20 - Read DS18B20 sensor (exterior temperature)
#------------------------------------------------------------------------
def readDS18B20():
	
	tempRead = -100.0
	cmdStat, cmdOut = commands.getstatusoutput("cat /sys/bus/w1/devices/28-000004d9ce4a/w1_slave")
	dbgprint("DS18B20 output: " + str(cmdOut))

	# Parse output
	if cmdStat == 0:
		matchObj = re.search(r'.*crc=\S*\s(\S*)[\n].*t=(.*)',cmdOut,re.I)
		if matchObj:
			if matchObj.group(1) == 'YES':
				tempRead = float(int(matchObj.group(2))/1000.0)
				dbgprint("Exterior Temperature: "+str(tempRead))
			else:
				dbgprint("DS18B20 returned CRC error.")
		else:
			dbgprint("No match during DS18B20 output parse!!")
	else:
		dbgprint("Error reading DS18B20 sensor")
	return tempRead

#------------------------------------------------------------------------
# readDHT11 - Read DHT11 sensor (Humidity)
#------------------------------------------------------------------------
def readDHT11():
	humRead = -100.0
	reading = 0
	while (humRead == -100.0 and reading < 10):	
		#try to read 10 times, because sometimes it get errors
		cmdStat, cmdOut = commands.getstatusoutput("/home/pi/projecto/Adafruit_DHT_Driver/Adafruit_DHT 11 17")
		dbgprint("DHT11 output: " + str(cmdOut))
	
		# Parse output
		if cmdStat == 0:
			matchObj = re.search(r'.*Hum = (.*)%',cmdOut,re.I)
			if matchObj:
				humRead = float(matchObj.group(1))
				dbgprint("Humidity: " + str(humRead))
			else:
				dbgprint("No match during DHT11 output parse!!")
		else:
			dbgprint("Error reading DHT11 sensor")
		reading += 1
	return humRead

#------------------------------------------------------------------------
# readBMP085 - Read BMP085 sensor (Interior temperature and atmospheric pressure)
#------------------------------------------------------------------------
def readBMP085():
	bmp = BMP085(0x77)
	tempiRead = bmp.readTemperature()
	dbgprint("Interior Temperature: " + str(tempiRead))
	pressRead = bmp.readPressure()/100.0
	dbgprint("Pressure: " + str(pressRead))
	return tempiRead, pressRead

#------------------------------------------------------------------------
# Main program
#------------------------------------------------------------------------

tempExt = readDS18B20()
humid = readDHT11()
tempInt, press = readBMP085()

#write to RRD archive
cmd = '/usr/bin/rrdtool update /home/pi/projecto/weatherRRD.rrd -t tempo:tempi:press:humid N:'
cmd = cmd +str(tempExt)+':'+str(tempInt)+':'+str(press)+':'+str(humid)
cmdStat, cmdOut = commands.getstatusoutput(cmd)
dbgprint("RRD write result: "+str(cmdStat))
dbgprint ("End!")
